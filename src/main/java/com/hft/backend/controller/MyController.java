package com.hft.backend.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MyController {

    @GetMapping("/test")
    public String getData() {
        return "Successful response from backend";
    }

    @GetMapping("date")
    public String getDate() {
        return new Date().toString();
    }

    @GetMapping("crash")
    public int crashApp() {
        System.exit(5);
        return 1;
    }

    @GetMapping("crash2")
    public int crashApp2() {
        Object[] o = null;

        while (true) {
            o = new Object[] {o};
        }
    }
}
